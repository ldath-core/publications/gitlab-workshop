from flask import Blueprint, jsonify

main = Blueprint('main', __name__)


@main.route('/', methods=['GET', 'OPTION'])
def index():
    return jsonify({'message': 'Hello my name is Backend'})
